class P(object):
    'Parent class'
    def __init__(self):                 # Parent constructor
        print 'Parent initialised'
    def __del__(self):                  # Parent destructor
        print 'Parent deleted'

class C(P):
    def __init__(self):                 # Subclass constructor
        P.__init__(self)                # Initialise parent class
        print 'Subclass initialised'
    def __del__(self):                  # Subclass destructor
        P.__del__(self)                 # Destroy parent class
        print 'Subclass deleted'
