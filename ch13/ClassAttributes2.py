class MyClass(object):
    'MyClass class definition. Determining class attributes.'
    myVersion = '1.1'           # static data
    def showMyVersion(self):    # method
        print MyClass.myVersion
