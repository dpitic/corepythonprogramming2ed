class MyClass:
    'Methods are class attributes, but must be called through instances'
    def myNoActionMethod(self):
        pass
