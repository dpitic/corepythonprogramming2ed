class AddrBookEntry(object):
    'address book entry class'
    def __init__(self, nm, ph):
        self.name = nm
        self.phone = ph
        print 'Created instance for:', self.name
    def updatePhone(self, newph):
        self.phone = newph
        print 'Updated phone# for:', self.name

class EmplAddrBookEntry(AddrBookEntry):
    'Employe Address Book Entry class'
    def __init__(self, nm, ph, eid, em):
        AddrBookEntry.__init__(self, nm, ph)
        self.empid = eid
        self.email = em
    def updateEmail(self, newem):
        self.email = newem
        print 'Updated e-mail address for:', self.name
