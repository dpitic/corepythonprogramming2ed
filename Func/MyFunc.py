__author__ = 'dpitic'

def addMe2Me(x):
    'apply + operation to argument'
    return x + x

def foo(debug=True):
    'determine if in debug mode with default argument'
    if debug:
        print 'In debug mode'
    print 'Done'